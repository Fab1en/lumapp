import serviceworker from 'serviceworker-webpack-plugin/lib/runtime';

/* eslint-disable no-console */
if (navigator.serviceWorker) {
	serviceworker.register({scope: process.env.VUE_APP_BASE_URL ? process.env.VUE_APP_BASE_URL+'/' : '/'}).then( function (reg) {
		console.log('Success!', reg);
	})
	.catch( function (e) {
		console.error('Failure!', e);
	});
}
