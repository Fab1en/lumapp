const DEBUG = true
const { assets } = global.serviceWorkerOption
const CACHE_NAME = 'lumapp'

// ajouter la racine de l'app aux assets
assets.push('./')
// transformer en URLs absolues
let assetsToCache = assets.map(path => {
	return new URL(path, global.location).toString()
})

// When the service worker is first added to a computer.
self.addEventListener('install', event => {
	// Perform install steps.
	if (DEBUG) {
		console.log('[SW] Install event')
	}

	// make service worker available without closing current tab
	global.skipWaiting()

	// notify client that a new version is available
	self.clients.matchAll().then(clients => {
		clients.forEach(client => {
			if (DEBUG) {
				console.log('an update is available')
			}
			client.postMessage('refresh')
		})
	})

	event.waitUntil(caches.open(CACHE_NAME).then(cache =>
		// cache all listed assets
		cache.addAll(global.serviceWorkerOption.assets)
	))
})

// After the install event.
self.addEventListener('activate', event => {
	if (DEBUG) {
		console.log('[SW] Activate event')
	}
	event.waitUntil(
		// delete all old caches
		caches.keys().then( cacheNames => Promise.all(
			cacheNames.map( cacheName => {
				if( cacheName != CACHE_NAME ) {
					return caches.delete(cacheName);
				}
			})
		)).then( () =>
			// get control over the client navigator
			clients.claim()
		)
	)
})

self.addEventListener('fetch', event => {
	const request = event.request
	let assets = global.serviceWorkerOption.assets.map(u => global.registration.scope + u.substring(2))

	// only if request is about a listed asset
	if( assets.includes(request.url) ){

		event.respondWith(
			caches.match(request).then(fromCache => {

				// fetch it again even when found in cache
				let fresh = fetch(request).then( response => {

					const copy = response.clone();
					if( !fromCache || copy.headers.get('ETag') != fromCache.headers.get('ETag') ) {
						// Put a copy in (or refresh) the cache
						caches.open(CACHE_NAME).then( cache => {
							cache.put(request, copy)
						})

						// notify client that a new version is available
						self.clients.matchAll().then(clients => {
							clients.forEach(client => {
								if (DEBUG) {
									console.log('an update is available')
								}
								client.postMessage('refresh')
							})
						})
					}
					return response
				}).catch(error => new Response('Oops! Something went wrong.'))

				// return cached response immediately or else freshly fetched one
				return fromCache || fresh
			})
		)

	}

})
