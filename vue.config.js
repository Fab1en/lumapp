var WebpackPwaManifest = require('webpack-pwa-manifest')
var ServiceWorkerWebpackPlugin = require('serviceworker-webpack-plugin');

module.exports = {
  outputDir: 'public',
  baseUrl:  process.env.VUE_APP_BASE_URL !== ''
    ? process.env.VUE_APP_BASE_URL
    : '/',
   pages: {
    index: {
      entry: 'src/main.js',
      template: 'templates/index.html'
    }
  },
  configureWebpack: {
    plugins: [
      new WebpackPwaManifest({
        lang: "en",
        name: "Lumapp",
        short_name: "Lumapp",
        description: "A tiny little flashlight PWA using VueJs",
        theme_color: "#727272",
        background_color : "#727272",
        start_url: (process.env.VUE_APP_BASE_URL ? process.env.VUE_APP_BASE_URL+'/' : '/'),
        display: "standalone",
        icons: [{
          src: 'src/assets/lightbulb.png',
          sizes: [96, 128, 192, 256, 384, 512] // multiple sizes
        }]
      }),
      new ServiceWorkerWebpackPlugin({
        entry: './src/go-service-worker.js',
        filename: 'go-service-worker.js',
        publicPath: (process.env.VUE_APP_BASE_URL ? process.env.VUE_APP_BASE_URL : '.') + '/'
      })
    ]
  },
  chainWebpack: config => {
    config
      .plugin('html-index')
      .tap(args => {
        // ajouter automatiquement la favicon
        args[0].favicon = 'src/assets/favicon.ico'
        return args
      })
  }
}
